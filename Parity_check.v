//Parity Check 奇偶校验位产生器
//制作者：FPGA研究者
//时间：2022年6月16日

module Parity_check(ain,even_bit,odd_bit);
     parameter WIDTH=8;
	  input [WIDTH-1:0]  ain;
	  output reg  even_bit,odd_bit;//奇校验位odd，为1时有效，0无效；偶校验位even，为1时有效，0无效；
always@(ain) begin
     
		odd_bit=^ain;//奇数个1，置1有效吗 
		even_bit=~odd_bit;//偶数个0，置1有效；
	end
endmodule
