////用时序逻辑电路设计J,K触发器，带异步清0.异步置1（低电平有效），J,K触发器，00：保持不变；01：低电平；10：高电平；11：取反。
//制作者：FPGA研究者
//时间：2022年6月16日

//未带异步清0.异步置1（低电平有效） 同步清0

module Dff_jk(j,k,clk,rst_n,q);
      input j,k,clk,rst_n;
		output reg q;
  always@(posedge clk) begin 
      if(!rst_n) begin
		   q<=1'b0;
		end
		else 
		 case({j,k})
		   2'b00: q<=q;
			2'b01:q<=1'b0;
			2'b10:q<=1'b1;
			2'b11:q<=~q;
			default :q<=1'bx;
		 endcase
		end
endmodule
			

/*
//带异步清0.异步置1（低电平有效）	
module Dff_jk(j,k,clk,rst_n,set,q);
      input j,k,clk,rst_n,set;
		output reg q;
  always@(posedge clk or negedge rst_n or negedge set) begin 
      if(!rst_n) begin
		   q<=1'b0;
		end
		else if(!set) begin
		   q<=1'b1;
		end
		else 
		 case({j,k})
		   2'b00: q<=q;
			2'b01:q<=1'b0;
			2'b10:q<=1'b1;
			2'b11:q<=~q;
			default :q<=1'bx;
		 endcase
		end
endmodule	
*/