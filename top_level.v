//TOP顶层文件，调用各个模块。
//制作者：FPGA研究者
//时间：2022年6月17日

module top_level(j,k,clk,rst_n,q,ain,even_bit,odd_bit,data_in,qout,select);
        input j,k,clk,rst_n;
		  input [7:0]  ain;
		  input [7:0] data_in;
		  input [1:0]select;
	     output  [7:0] qout;
	     output  even_bit,odd_bit;//奇校验位odd，为1时有效，0无效；偶校验位even，为1时有效，0无效；
		  output  q;
		 Dff_jk f0(j,k,clk,rst_n,q); //JK触发器
	    Parity_check f1(ain,even_bit,odd_bit);//奇偶校验位
		 reg8 f2(data_in,clk,qout,rst_n);//8位寄存器 
		 //只能把模块直接放置，不能分情况
	 
/*		  

always@(posedge clk) begin 
       if(!rst_n) begin
		   qout=0;
			even_bit=0;
			odd_bit=0;
			q=0;
			end

//always@(select)
		
		     Dff_jk f0(j,k,clk,rst_n,q);//00时为JK触发器
			  Parity_check f1(ain,even_bit,odd_bit);//01时为奇偶校验位
			 reg8 f2(data_in,clk,qout,rst_n);//10时8位寄存器 
			 // 2'b11:begin  qout=1;even_bit=1;odd_bit=1;q=1;end //11时所有输出的数据都为1。
			
*/
endmodule
			
			